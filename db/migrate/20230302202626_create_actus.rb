# Store feed items
class CreateActus < ActiveRecord::Migration[7.0]
  def change
    create_table :actus do |t|
      t.integer :orga_id, null: false, foreign_key: true
      t.string :title, :link, :guid
      t.text :description
      t.datetime :published_at

      t.timestamps
    end
    add_index :actus, :guid
  end
end
