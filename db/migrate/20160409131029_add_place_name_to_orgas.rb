# Add more data to orgas, with an optionnal place name and address
class AddPlaceNameToOrgas < ActiveRecord::Migration
  def change
    add_column :orgas, :place_name, :text
    add_column :orgas, :address, :text
    add_column :orgas, :latitude, :float
    add_column :orgas, :longitude, :float
  end
end
