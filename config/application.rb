require_relative 'boot'

require 'rails/all'
require 'action_view/encoded_mail_to/mail_to_with_encoding'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module AgendaDuLibreRails
  # All the specific configuraton for ADL
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.1

    config.i18n.load_path +=
      Dir[Rails.root.join('config/locales/**/*.{rb,yml}')]
    # config.i18n.default_locale = :de
    config.i18n.default_locale = :fr
    config.i18n.available_locales = %i[de en fr nl pt-BR]
    config.i18n.fallbacks = %i[en fr]

    config.action_mailer.default_options = {
      from: ENV.fetch('ADL_MAIL', 'moderateurs@agendadulibre.org'),
      to: ENV.fetch('ADL_MAIL', 'moderateurs@agendadulibre.org')
    }

    # In rails 4, plugin and vendor images need to be precompiled
    config.assets.precompile += %w[*.png *.jpg *.jpeg *.gif]

    # Rails 5 now defaults to csrf tokens per form, which breaks adl for the
    # time being
    # TODO
    config.action_controller.per_form_csrf_tokens = false

    config.cloud_threshold = 5

    config.tinymce.install = :copy

    # A security mitigation, withouth which paper_trail "previous_version" doesn't work anymore
    # visible in mail update previews...
    # https://github.com/rails/rails/commit/6576aa7bbcf52ebd39853363e29f92b4dd53b6f1
    # https://discuss.rubyonrails.org/t/cve-2022-32224-possible-rce-escalation-bug-with-serialized-columns-in-active-record/81017
    config.active_record.yaml_column_permitted_classes =
      [Symbol, Time, ActiveSupport::TimeWithZone, ActiveSupport::TimeZone,
       ActsAsTaggableOn::TagList, ActsAsTaggableOn::DefaultParser]

    # Please, add to the `ignore` list any other `lib` subdirectories that do
    # not contain `.rb` files, or that should not be reloaded or eager loaded.
    # Common ones are `templates`, `generators`, or `middleware`, for example.
    config.autoload_lib(ignore: %w[assets tasks])

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")
  end
end
