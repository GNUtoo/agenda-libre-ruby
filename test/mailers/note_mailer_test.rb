require 'test_helper'

# Test that some information is sent to submitter and moderators when notes are
# added to events
class NoteMailerTest < ActionMailer::TestCase
  setup do
    ActionMailer::Base.default_url_options[:host] = 'localhost:3000'
  end

  test 'notify' do
    mail = NoteMailer.notify Note.last

    assert_match(/Demande d'informations sur l'événement.*/,
                 mail.subject)
    assert_equal [Note.last.event.contact], mail.to
    assert_equal ['moderateurs@agendadulibre.org'], mail.from
  end

  test 'create' do
    mail = NoteMailer.create Note.last

    assert_match(/Une note a été rajoutée à l'événement.*/,
                 mail.subject)
    assert_not_equal [Note.last.event.contact], mail.to
    assert_equal ['moderateurs@agendadulibre.org'], mail.from
  end
end
