require 'test_helper'

# Test mails sent related to event moderation
class ModerationMailerTest < ActionMailer::TestCase
  setup do
    ActionMailer::Base.default_url_options[:host] = 'localhost:3000'

    @config = Rails.application.config
  end

  test 'create' do
    mail = ModerationMailer.create Event.unscoped.last

    assert_match(/Nouvel événement à modérer: .*/,
                 mail.subject)
    assert_equal ['moderateurs@agendadulibre.org'], mail.to
    assert_equal ['moderateurs@agendadulibre.org'], mail.from
  end

  test 'update' do
    event = Event.last

    # Added so paper trail can have some bit of history
    event.save

    event.tag_list.add 'ho'
    event.start_time += 1.day
    event.description = event.description + '
hello world'

    mail = ModerationMailer.update event

    assert_match(/Édition de l'événement .*/, mail.subject)
    assert_match(/^-/, mail.body.to_s)
    assert_match(/^+/, mail.body.to_s)
  end

  test 'accept' do
    mail = ModerationMailer.accept Event.unscoped.last

    assert_match(/Événement .* modéré/, mail.subject)
    assert_equal ['moderateurs@agendadulibre.org'], mail.to
    assert_equal ['moderateurs@agendadulibre.org'], mail.from
  end

  test 'destroy' do
    event = Event.unscoped.last
    event.reason = 'hello world'
    mail = ModerationMailer.destroy event

    assert_match(/Événement .* refusé/, mail.subject)
    assert_equal ['moderateurs@agendadulibre.org'], mail.to
    assert_equal ['moderateurs@agendadulibre.org'], mail.from
  end
end
