require 'test_helper'

# Test and check how the organisations mailer is working
class OrgaMailerTest < ActionMailer::TestCase
  setup do
    ActionMailer::Base.default_url_options[:host] = 'localhost:3000'

    @config = Rails.application.config
  end

  test 'create' do
    mail = OrgaMailer.create Orga.last

    assert_match(/Organisation .* en attente de modération/,
                 mail.subject)
    assert_equal [Orga.last.submitter], mail.to
    assert_emails 1 do
      mail.deliver_now
    end
  end

  test 'update' do
    @orga = Orga.last
    # Necessary to have the proper paper_trail version
    @orga.update name: 'My Title'

    mail = OrgaMailer.update @orga

    assert_match(/Organisation .* modifiée/, mail.subject)
    assert_equal [Orga.last.submitter], mail.to
    assert_emails 1 do
      mail.deliver_now
    end
  end

  test 'update withour mail' do
    assert_emails 1 do
      Orga.last.update contact: nil, submitter: nil
    end
  end

  test 'accept' do
    mail = OrgaMailer.accept Orga.last

    assert_match(/Organisation .* modérée/, mail.subject)
    assert_equal [Orga.last.submitter], mail.to
    assert_emails 1 do
      mail.deliver_now
    end
  end

  test 'destroy' do
    mail = OrgaMailer.destroy Orga.last, 'hello world'

    assert_match(/Organisation .* supprimée/, mail.subject)
    assert_equal [Orga.last.submitter], mail.to
    assert_emails 1 do
      mail.deliver_now
    end
  end
end
