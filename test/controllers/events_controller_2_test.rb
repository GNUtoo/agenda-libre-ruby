require 'test_helper'

# Event life cycle
class EventsController2Test < ActionDispatch::IntegrationTest
  test 'should get index' do
    get events_url

    assert_response :success
    assert_not_nil assigns :events
  end

  test 'should get index as ics' do
    get events_url format: :ics

    assert_response :success
    assert_not_nil assigns :events

    assert_match(/TZID=/, response.body)
  end
end
