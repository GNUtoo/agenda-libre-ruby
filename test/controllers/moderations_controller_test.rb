require 'test_helper'

# Event management, moderation means refusal, acceptation or demands for more information
class ModerationsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @moderation = events :one

    sign_in users(:one)
  end

  test 'should get index' do
    get moderations_url

    assert_response :success
    assert_not_nil assigns(:events)
  end

  test 'should preview event' do
    assert_no_difference 'Event.moderated.count' do
      patch moderation_url(@moderation), params: {
        event: {
          title: 'hello world',
          region: regions(:region_one)
        }
      }
    end

    assert_redirected_to moderations_url
  end

  test 'should edit event' do
    assert_no_difference 'Event.moderated.count' do
      put moderation_url(@moderation), params: {
        event: {
          title: 'hello world',
          region: regions(:region_one)
        }
      }
    end

    assert_redirected_to moderations_url
  end

  test 'should not edit event' do
    assert_no_difference 'Event.moderated.count' do
      put moderation_url(@moderation), params: { event: { title: nil } }
    end

    assert_not_empty assigns(:moderation).errors
  end

  test 'should accept event' do
    assert_changes 'events(:proposed).reload.decision_time' do
      assert_difference 'Event.moderated.count' do
        put accept_moderation_url events :proposed
      end
    end

    assert_redirected_to :moderations
  end

  test 'should update event' do
    # Added so paper trail can have some bit of history
    patch moderation_url(@moderation), params: { event: { title: 'hop hop' } }
    patch moderation_url(@moderation), params: {
      event: {
        title: @moderation.title,
        start_time: @moderation.start_time, end_time: @moderation.end_time,
        description: @moderation.description,
        url: @moderation.url,
        contact: @moderation.contact
      }
    }

    assert_empty assigns(:moderation).errors
    assert_redirected_to :moderations
  end

  test 'should not update event' do
    patch moderation_url(@moderation), params: { event: { title: nil } }

    assert_not_empty assigns(:moderation).errors
  end

  test 'can not update event concurrently' do
    patch moderation_url(@moderation), params: {
      event: { lock_version: @moderation.lock_version - 1 }
    }

    assert_redirected_to edit_moderation_path @moderation
  end

  test 'should reject spam' do
    assert_difference 'Event.count', -1 do
      delete moderation_url(@moderation), params: {
        reason: 'r_0', event: { reason: '' }
      }
    end

    assert_not_empty assigns(:moderation).reason
    assert_redirected_to :moderations
  end

  test 'should reject event' do
    assert_difference 'Event.count', -1 do
      delete moderation_url(@moderation), params: {
        reason: 'r_1', event: { reason: '' }
      }
    end

    assert_not_empty assigns(:moderation).reason
    assert_redirected_to :moderations
  end

  test 'should reject event with a reason' do
    assert_difference 'Event.count', -1 do
      delete moderation_url(@moderation), params: {
        reason: 'r_4', event: { reason: 'bye' }
      }
    end

    assert_equal 'bye', assigns(:moderation).reason
    assert_redirected_to :moderations
  end
end
