require 'test_helper'

# Event life cycle
class EventsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @event = events :one
  end

  test 'should get new' do
    get new_event_url

    assert_response :success
  end

  test 'should preview event creation' do
    assert_no_difference 'Event.count' do
      post preview_events_url, params: { event: {
        title: @event.title,
        start_time: @event.start_time, end_time: @event.end_time,
        description: @event.description,
        city: @event.city, region_id: @event.region.id,
        url: @event.url, submitter: @event.contact, tag_list: 'helo world'
      } }

      assert_empty assigns(:event).errors
    end

    assert_response :success
  end

  test 'should create event' do
    assert_difference 'Event.count' do
      post events_url, params: { event: {
        title: @event.title,
        start_time: @event.start_time, end_time: @event.end_time,
        description: @event.description,
        city: @event.city, region_id: @event.region.id,
        url: @event.url, submitter: @event.contact, tag_list: 'helo world'
      } }

      assert_empty assigns(:event).errors.messages
    end

    assert_redirected_to controller: 'events', action: 'show',
                         id: assigns(:event).id, secret: assigns(:event).secret
  end

  test 'should not create event' do
    assert_no_difference 'Event.count' do
      post events_url, params: {
        event: { title: @event.title, start_time: @event.start_time }
      }

      assert_not_empty assigns(:event).errors.messages
    end
  end

  test 'should show event' do
    get event_url(@event)

    assert_response :success
  end

  test 'should get edit' do
    get edit_event_url(@event), params: { secret: @event.secret }

    assert_response :success
  end

  test 'should not get edit' do
    get edit_event_url(@event)

    assert_redirected_to :root
  end

  test 'should preview' do
    assert_no_difference 'Event.count' do
      patch preview_event_url(@event), params: {
        secret: @event.secret, event: { title: @event.title }
      }

      assert_empty assigns(:event).errors
    end

    assert_response :success
  end

  test 'should update event' do
    patch event_url(@event), params: {
      secret: @event.secret, event: { title: @event.title }
    }

    assert_empty assigns(:event).errors.messages
    assert_redirected_to event_url(@event)
  end

  test 'should not update event' do
    patch event_url(@event), params: {
      secret: @event.secret, event: { title: nil }
    }

    assert_not_empty assigns(:event).errors.messages
  end

  test 'can not update event concurrently' do
    patch event_url(@event), params: {
      secret: @event.secret, event: { lock_version: @event.lock_version - 1 }
    }

    assert_redirected_to edit_event_url(@event, secret: @event.secret)
  end

  test 'should get cancel page' do
    get cancel_event_url(@event), params: { secret: @event.secret }

    assert_response :success
  end

  test 'should destroy event' do
    assert_difference('Event.count', -1) do
      delete event_url(@event), params: {
        secret: @event.secret, event: { reason: 'bye' }
      }
    end

    assert_equal 'bye', assigns(:event).reason
    assert_redirected_to :root
  end
end
