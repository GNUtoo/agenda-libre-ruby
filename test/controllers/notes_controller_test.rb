require 'test_helper'

# Test the notes life cycle
class NotesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @note = notes(:one)

    sign_in users(:one)
  end

  test 'should get new' do
    get new_moderation_note_url(@note.event)

    assert_response :success
  end

  test 'should create note' do
    assert_difference 'Note.count' do
      post moderation_notes_url(@note.event), params: {
        note: { contents: @note.contents }
      }

      assert_empty assigns(:note).errors.messages
    end

    assert_redirected_to moderations_path
  end

  test 'should send mail' do
    assert_difference 'Note.count' do
      post moderation_notes_url(@note.event), params: {
        envoiParMail: 'oui',
        note: { contents: @note.contents }
      }
    end

    assert_predicate ActionMailer::Base.deliveries, :present?

    assert_redirected_to moderations_path
  end

  test 'should not create note' do
    assert_no_difference 'Note.count' do
      post moderation_notes_url(@note.event), params: {
        note: { nothing: 'almost' }
      }

      assert_not_empty assigns(:note).errors.messages
    end
  end
end
