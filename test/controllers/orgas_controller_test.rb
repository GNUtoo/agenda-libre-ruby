require 'test_helper'

# Organisations life cycle tests
class OrgasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @orga = orgas :one
  end

  test 'should get index' do
    get orgas_url, params: {
      orga: {
        region: regions(:region_one)
      }
    }

    assert_response :success
  end

  test 'should get new' do
    get new_orga_url

    assert_response :success
  end

  test 'should preview orga creation' do
    assert_no_difference 'Orga.count' do
      post preview_orgas_url, params: { orga: {
        kind_id: @orga.kind_id, name: @orga.name,
        city: @orga.city, region_id: @orga.region.id,
        description: @orga.description,
        url: @orga.url, contact: @orga.contact, tag_list: 'helo world'
      } }

      assert_empty assigns(:orga).errors
    end

    assert_response :success
  end

  test 'should create orga' do
    assert_difference 'Orga.count' do
      post orgas_url, params: { orga: {
        kind_id: @orga.kind_id, name: @orga.name,
        city: @orga.city, region_id: @orga.region.id,
        description: @orga.description,
        url: @orga.url, feed: @orga.feed,
        contact: @orga.contact, submitter: @orga.contact
      } }
    end

    assert_redirected_to controller: 'orgas', action: 'show',
                         id: assigns(:orga).id, secret: assigns(:orga).secret
  end

  test 'should create minimalist orga' do
    assert_difference 'Orga.count' do
      post orgas_url, params: { orga: {
        kind_id: @orga.kind_id,
        name: @orga.name,
        region_id: @orga.region.id,
        description: @orga.description,
        url: @orga.url
      } }
    end

    assert_redirected_to controller: 'orgas', action: 'show',
                         id: assigns(:orga).id, secret: assigns(:orga).secret
  end

  test 'should create orga without content' do
    assert_difference 'Orga.count' do
      post orgas_url, params: { orga: {
        kind_id: @orga.kind_id,
        name: @orga.name,
        region_id: @orga.region.id,
        url: @orga.url
      } }
    end

    assert_redirected_to controller: 'orgas', action: 'show',
                         id: assigns(:orga).id, secret: assigns(:orga).secret
  end

  test 'should not create orga' do
    assert_no_difference 'Orga.count' do
      post orgas_url, params: { orga: { url: @orga.url } }
    end
  end

  test 'should get show' do
    get orga_url(@orga.id)

    assert_response :success
  end

  test 'should preview' do
    assert_no_difference 'Orga.count' do
      patch preview_orga_url(@orga), params: {
        secret: @orga.secret, orga: { name: @orga.name }
      }

      assert_empty assigns(:orga).errors
    end

    assert_response :success
  end

  test 'should update orga' do
    # Necessary to have the proper paper_trail version
    @orga.update name: 'My Title'

    patch orga_url(@orga), params: {
      secret: @orga.secret,
      orga: { name: @orga.name }
    }

    assert_redirected_to orga_url(@orga)
  end

  test 'should not update orga' do
    patch orga_url(@orga), params: { orga: { name: nil } }

    assert_redirected_to :new_user_session
  end

  test 'should not update orga without proper secret' do
    patch orga_url(@orga), params: { orga: { name: 'hello world' } }

    assert_redirected_to :new_user_session
  end
end
