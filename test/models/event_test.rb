require 'test_helper'

# Test events, which are the application central part
class EventTest < ActiveSupport::TestCase
  setup do
    ActionMailer::Base.default_url_options[:host] = 'localhost:3000'

    @event = events :one
  end

  SIZE_32_ATTRIBUTES = %w[secret moderator_mail_id submitter_mail_id].freeze

  test 'basic event' do
    @event = Event.new(
      title: 'hello world',
      start_time: Time.zone.now,
      end_time: 1.hour.from_now,
      description: 'et hop!',
      region: Region.first,
      submitter: 'submitter@example.com',
      tag_list: 'hello world'
    )
    assert_difference 'Event.count' do
      @event.save!
    end

    assert_empty @event.errors

    SIZE_32_ATTRIBUTES.each { |attribute| assert_equal 32, @event.send(attribute).size }
  end

  test 'validations' do
    @event = Event.new(
      title: 'hello world',
      start_time: Time.zone.now,
      end_time: 1.hour.from_now,
      description: 'et hop!',
      region: Region.first,
      submitter: 'contact@example.com',
      tag_list: 'hello world'
    )

    assert_predicate @event, :valid?, @event.errors.messages

    @event.contact = 'hop'

    assert_not @event.valid?, @event.errors.messages

    @event.contact = 'contact@example.com'

    assert_predicate @event, :valid?, @event.errors.messages
  end

  test 'invalid url' do
    @event = Event.new(
      title: 'hello world',
      start_time: Time.zone.now,
      end_time: 1.hour.from_now,
      description: 'et hop!',
      region: Region.first,
      submitter: 'contact@example.com',
      tag_list: 'hello world'
    )

    # Check invalid url
    @event.url = 'htt://truc.com'

    assert_not @event.valid?, @event.errors.messages

    @event.url = 'http:/truc.com'

    assert_not @event.valid?, @event.errors.messages
  end

  test 'moderation' do
    @event = Event.new(
      title: 'hello world',
      start_time: Time.zone.now,
      end_time: 1.hour.from_now,
      description: 'et hop!',
      region: Region.first,
      submitter: 'contact@example.com',
      tag_list: 'hello world'
    )

    @event.save

    assert_empty @event.errors.messages
    assert_not @event.moderated?

    @event.update moderated: 1

    assert_predicate @event, :moderated?, @event.errors.messages
  end

  test 'named scope future.daylimit' do
    assert_respond_to Event, :future
    assert_respond_to Event, :daylimit
    assert_match(/<= end_time\) AND \(end_time <=/, Event.future.daylimit(nil).to_sql)
  end

  test 'named scope year' do
    assert_respond_to Event, :year
    assert_match(/<= end_time/, Event.year(2014).to_sql)
    assert_match(/start_time <=/, Event.year(2014).to_sql)
  end

  test 'named scope region' do
    assert_respond_to Event, :region
    assert_not_nil Event.region Region.first.id
  end

  test 'geo data reset' do
    # Setup geo data
    @event.latitude = 3
    @event.longitude = 3

    # Change address to ensure geo data is reset
    @event.address = 'hello world'

    assert_predicate @event, :valid?, @event.errors.messages
    assert_in_delta(40.7143528, @event.latitude)
    assert_in_delta(-74.0059731, @event.longitude)
  end

  test 'full address' do
    @event.address = 'hello'
    @event.city = 'world'
    @event.region.name = 'here'
    @event.region.code = 'xyz'

    assert_equal 'hello, world, here, France', @event.full_address
  end
end
