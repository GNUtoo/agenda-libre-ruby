require 'test_helper'

# Some checks on actualities
class ActuTest < ActiveSupport::TestCase
  test 'Check publication date is not in the future' do
    @actu = Actu.new(
      orga: Orga.first,
      guid: 'abcdef',
      title: 'hello world',
      link: 'https://www.agendadulibre.org',
      description: 'lorem ipsum',
      published_at: 2.days.from_now
    )

    assert_difference 'Actu.count' do
      @actu.save
    end
    assert_equal @actu.created_at, @actu.published_at
  end
end
