require 'test_helper'

# Some tests for moderators
class UserTest < ActiveSupport::TestCase
  test 'basic user' do
    @user = User.new(
      email: 'original@example.com',
      firstname: 'first',
      lastname: 'last',
      login: 'login',
      password: 'abcdefghijklmnopqrstuvwxyz'
    )

    assert_not_nil @user
    assert_equal 'c3fcd3d76192e4007dfb496cca67e13b', @user.encrypted_password
  end

  test 'the password' do
    @user = User.new(
      email: 'original@example.com',
      firstname: 'first',
      lastname: 'last',
      login: 'login',
      password: 'abcdefghijklmnopqrstuvwxyz'
    )

    assert @user.valid_password? 'abcdefghijklmnopqrstuvwxyz'
  end
end
