require 'test_helper'

# Mostly a data holder model, to maintain coutry > region data
class RegionTest < ActiveSupport::TestCase
  test 'proper timezone' do
    assert_predicate regions(:region_one).tzid, :present?
    assert_equal 'Europe/Paris', regions(:region_one).tzid
  end

  test 'check québec timezone is local timezone' do
    assert_equal Time.now.zone, regions(:region_quebec).tzid
  end

  test 'check other timezone is local timezone' do
    assert_match(/CE.*/, regions(:region_other).tzid)
  end
end
