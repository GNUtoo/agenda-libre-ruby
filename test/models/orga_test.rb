require 'test_helper'

# Verify the organisation creation and mail workflow
class OrgaTest < ActiveSupport::TestCase
  setup do
    ActionMailer::Base.default_url_options[:host] = 'localhost:3000'

    @config = Rails.application.config
  end

  test 'propose orga' do
    assert_difference 'ActionMailer::Base.deliveries.size', 2 do
      Orga.create!(
        kind: Kind.first,
        name: 'Tested organisation',
        url: 'http://example.com',
        region: Region.first,
        description: 'helo world',
        contact: 'contact@example.com',
        submitter: 'contact@example.com',
        tag_list: 'hello world'
      )
    end
  end

  test 'set and send secret' do
    @orga = orgas(:two)

    assert_nil @orga.secret
    assert_difference 'ActionMailer::Base.deliveries.size', 3 do
      @orga.name = 'hop hop hop'
      @orga.save!
    end
    assert_not_nil @orga.secret
  end

  test 'edit orga' do
    @orga = orgas(:one)
    assert_difference 'ActionMailer::Base.deliveries.size', 2 do
      @orga.name += ' addition added as an edit :)'
      @orga.save!
    end
  end

  test 'accept orga' do
    @orga = orgas(:two)
    assert_difference 'ActionMailer::Base.deliveries.size', 3 do
      @orga.moderated = true
      @orga.save!
    end
  end

  test 'refuse orga' do
    @orga = orgas(:two)
    assert_difference 'ActionMailer::Base.deliveries.size', 2 do
      @orga.destroy!
    end
  end
end
