require 'test_helper'

# Test some event helper methods
class EventsHelperTest < ActionView::TestCase
  MD_EMPTY_CONTENT = [
    '',
    '  ',
    '
    ',
    '<strong><img src=""/></strong>'
  ].freeze

  test 'Markdown with empty content' do
    MD_EMPTY_CONTENT.map { |content| to_markdown content }
                    .each { |content| assert_empty content }

    assert_equal "'", to_markdown("'")
  end

  WITH_SPACE = [
    ['<strong>hello world </strong>', '**hello world**'],
    ['<strong>hello world    </strong>', '**hello world**'],
    ['<strong> h</strong>,', '**h**,'],
    ['<strong>h </strong>,', '**h**,'],
    ['<em>ho</em><strong>h </strong>,', '*ho***h**,'],
    ['<em>ho</em><strong> h</strong>,', '*ho* **h**,'],
    ['<i>ho</i><b> h</b>,', '*ho* **h**,'],
    ['<i>ho</i><b> h<br></b>,', '*ho* **h**,'],
    ['<i>ho</i><b> h<br/></b>,', '*ho* **h**,']
  ].freeze

  test 'HTML elements with a space inside' do
    WITH_SPACE.each { |html, md| assert_equal md, to_markdown(html) }
  end

  STRONG_TITLES = [
    ['<h1>Big</h1>', '**Big**

'],
    ['<h2>Big again</h2>', '**Big again**

'],
    ['<h3><strong>Too</strong></h3>', '**Too**

']
  ].freeze

  test 'HTML titles too strong' do
    STRONG_TITLES.each { |html, md| assert_equal md, to_markdown(html) }
  end
end
