source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

# The central piece of this application: the month calendar view
gem 'simple_calendar'

gem 'icalendar'

# The recurrence management library
gem 'ice_cube'

gem 'rails'
gem 'sprockets-rails', require: 'sprockets/railtie'

# Helper to generate json
gem 'jbuilder'

gem 'haml-rails'

gem 'devise'
gem 'devise-i18n'
gem 'http_accept_language'
gem 'rails-i18n'
# Store some specific content, like application name and static pages contents
gem 'i18n-active_record', require: 'i18n/active_record'
gem 'i18n-tasks'
# Remove leading and trailing white spaces
gem 'strip_attributes'
# Track changes to models' data
gem 'paper_trail'

# Validate mails submitted
gem 'email_validator'
# Email address obfuscation
gem 'actionview-encoded_mail_to'
# To display differences
gem 'diffy'

# A generic library to administrate the tool
gem 'activeadmin'

gem 'has_scope'
gem 'preserve'

# Markdown display
gem 'kramdown'

gem 'flag-icons-rails'
gem 'geocoder'
gem 'i18n-country-translations'

# Tiny MCE integration
gem 'tinymce-rails'

gem 'acts-as-taggable-on'
gem 'tagsinput-rails'

# Rails engine for static pages
gem 'high_voltage'

# SEO optimisations
gem 'meta-tags'

gem 'rss'

group :development, :test do
  # Use sqlite3 as the database for Active Record
  gem 'sqlite3'
  # Call 'byebug' anywhere in the code to stop execution and get a debugger
  # console
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
end

group :development do
  gem 'better_errors'
  gem 'binding_of_caller'
  # Security checks
  gem 'guard-brakeman', require: false
  gem 'guard-bundler', require: false
  gem 'guard-rails', require: false
  # Scanning the Gemfile for insecure dependencies based on published CVEs
  gem 'guard-bundler-audit', require: false
  gem 'guard-livereload', require: false
  gem 'guard-migrate', require: false
  gem 'guard-minitest', require: false
  gem 'guard-rubocop', require: false
  # Use Puma as the app server
  gem 'puma'
  gem 'rack-livereload'
  gem 'rubocop-minitest'
  gem 'rubocop-performance'
  gem 'rubocop-rails'
  # Spring speeds up development by keeping your application
  # running in the background. Read more:
  # https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen'
  # Access an IRB console on exception pages or by using <%= console %>
  # anywhere in the code.
  gem 'web-console'
end

group :test do
  # To test objects assigned in controllers, much easier...
  gem 'rails-controller-testing'
end

group :development, :test, :assets do
  # Use CoffeeScript for .coffee assets and views
  gem 'coffee-rails'
  # Use SASS for stylesheets
  gem 'sassc-rails'

  # Use jquery and jquery-ui as the JavaScript library
  gem 'jquery-ui-rails'
  # Turbolinks makes following links in your web application
  # faster. Read more: https://github.com/rails/turbolinks
  gem 'turbolinks'

  gem 'modernizr-rails'

  # A superb font to use as icons
  gem 'font-awesome-sass'

  source 'https://rails-assets.org' do
    gem 'rails-assets-iframe-resizer'
    gem 'rails-assets-jquery-sparkline'
    gem 'rails-assets-jquery-visible'
    # More recent versions do not build the proper js themself
    gem 'rails-assets-leaflet', '1.7.1'
    gem 'rails-assets-leaflet.markercluster', '1.4.1'
  end
end

group :assets do
  # Use Uglifier as compressor for JavaScript assets
  gem 'uglifier'
end

group :production do
  # Use postgresql as the database for Active Record
  # gem 'pg'
  # Use mysql as the database for Active Record
  gem 'mysql2'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
