# Cleans up filter submission, for cleaner URL
$(document).on 'turbolinks:load', ->
	$('body.pages form :input').prop 'disabled', false

	$('form').submit ->
		$('input[name=utf8]').prop 'disabled', true
		$('button').prop 'disabled', true

	# Closes dropdown when checking an element
	# x is a hack to prevent the blur when using keyboard
	# ending with _, normally first element, prevents the blur
	$('.region :input').click (e) ->
		e.originalEvent.x && !e.target.id.endsWith('_') && e.target.blur()
