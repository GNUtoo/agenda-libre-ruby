$(document).on 'turbolinks:load', ->
  # Sparline generation along the data tables
	$('table.list.dates tbody .sparkline').each ->
		vals = $(this).closest('tr').find('.quantity').map ->
			$(this).text()?.replace(' ', '') || 0
		$(this).sparkline vals

	$('table.list.dates tfoot .sparkline').each ->
		vals = $(this).closest('tfoot').find('.quantity').map ->
			$(this).text().replace(' ', '') || 0

		$(this).sparkline vals, type: 'bar'
