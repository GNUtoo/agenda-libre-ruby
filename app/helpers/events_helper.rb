# Helper for the event views
module EventsHelper
  def events_meta
    set_meta_tags \
      prev: prev_url,
      next: next_url,
      description: t('layouts.application.subtitle'),
      keywords: params[:tag],
      DC: {
        title: t('layouts.application.title'),
        subject: t('layouts.application.subtitle'),
        publisher: 'april'
      }
  end

  def event_meta(event)
    return if event.new_record?

    set_meta_tags \
      keywords: event.tag_list.to_s,
      DC: { title: event.title, date: event.start_time.to_s },
      og: { image: first_image(event.description), title: event.title, type: 'website' },
      geo: geo(event),
      canonical: event_url(event)
  end

  def display_date(event)
    if event.start_time.to_date == event.end_time.to_date
      display_sameday event
    else
      display_multi_days event
    end
  end

  def display_sameday(event)
    t 'date.formats.same_day',
      date: l(event.start_time.to_date, format: :at),
      start: l(event.start_time, format: :hours),
      end: l(event.end_time, format: :hours)
  end

  def display_multi_days(event)
    t 'date.formats.period', start: l(event.start_time, format: :at), end: l(event.end_time, format: :at)
  end

  # Select the events to display in a month
  def month_events(events, date)
    events.select { |e| (e.start_time.to_date..e.end_time.to_date).cover? date }
  end

  def display_attr(item, label, value = item[label])
    # return unless value
    item.class.human_attribute_name(label).rjust(12) + " #{value}"
  end

  # Using kramdown, let's parse the html and render it as markdown text
  def to_markdown(desc, line_width = -1)
    return '' if desc.blank?

    post Kramdown::Document.new(pre(desc), input: :html, line_width: line_width)
                           .to_kramdown
  end

  # Copy some attributes to duplicate an event
  def duplicate(event)
    event.attributes
         .slice(*%w[title start_time end_time place_name address city region_id locality description url contact])
         .merge(tag_list: event.tags.join(' '))
  end

  private

  def prev_url
    if params[:year]
      url_for year: params[:year].to_i - 1
    else
      url_for start_date: (params[:start_date] || Time.zone.today).to_date.beginning_of_month.beginning_of_week.prev_day
    end
  end

  def next_url
    if params[:year]
      url_for year: params[:year].to_i + 1
    else
      url_for start_date: (params[:start_date] || Time.zone.today).to_date.end_of_month.end_of_week.next_day
    end
  end

  def first_image(description)
    Nokogiri::HTML.fragment(description)
                  .css('img')
                  .first
                  .try(:[], :src)
  end

  def geo(event)
    position = [event.latitude, event.longitude].select(&:present?)
    {
      placename: event.city, region: event.region.try(:to_s),
      position: position.join(';'), ICBM: position.join(', ')
    }
  end

  TAGS_TO_SANITIZE = %w[p br h1 h2 h3 h4 table tr th td ul ol li a strong b em i].freeze

  # Cleaner html elements, to correct things like <em> test</em>
  def pre(desc)
    desc = sanitize desc, tags: TAGS_TO_SANITIZE, attributes: %w[href]

    desc.gsub(/<(strong|em|b|i)>\s+/, ' <\1>')
        .gsub(%r{\s+</(strong|em|b|i)>}, '</\1> ')
        .gsub(/\s+([,.:])/, '\1')
        .gsub(%r{<br/?></}, '</') # Remove extraneous breaks
        .remove(%r{<(strong|em|b|i)\s*/>})
        .remove(%r{<(strong|em|b|i)>\s*</(strong|em|b|i)>})
        .remove(%r{<(strong|em|b|i)>\s*</(strong|em|b|i)>}) # Twice to remove eventually imbricated ones
  end

  def post(generated)
    generated.gsub(/^#+\s+(.*)/, '**\1**') # Title transformed into strong
             .gsub('****', '**')
             .gsub(/\\(["'])/, '\1') # Remove slash before quotes
             .remove(/[[:blank:]]+$/) # Remove extraneous spaces
             .remove(/{::}/) # Markdown artefact
             .remove(/^\*\*$\n/)
  end
end
