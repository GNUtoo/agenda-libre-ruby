# Helper for the orga views
module OrgasHelper
  def orga_meta(orga)
    return if orga.new_record?

    set_meta_tags \
      keywords: orga.tags.join(' '),
      DC: { title: orga.name },
      geo: geo(orga),
      canonical: orga_url(orga)
  end

  def geo(orga)
    position = [orga.latitude, orga.longitude].select(&:present?)
    {
      placename: orga.city, region: orga.region.try(:to_s),
      position: position.join(';'),
      ICBM: position.join(', ')
    }
  end
end
