json.array!(@regions) do |region|
  json.extract! region, :id, :region_id, :name, :code
end
