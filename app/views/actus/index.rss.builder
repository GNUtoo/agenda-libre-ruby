def meta(xml, actu)
  xml.dc :identifier, "actu_#{actu.id}@#{request.domain}"

  return unless actu.orga.latitude || actu.orga.longitude

  xml.georss :point, actu.orga.latitude, ' ', actu.orga.longitude
end

def actu_to_rss(xml, actu)
  meta xml, actu

  xml.title "#{actu.orga.name}: #{actu.title}"
  xml.pubDate actu.created_at.iso8601
  xml.link actu.link
  xml.description actu.description
  xml.content(:encoded) { xml.cdata! actu.description }
end

xml.instruct!

xml.rdf :RDF,
        'xmlns' => 'http://purl.org/rss/1.0/',
        'xmlns:rdf' => 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
        'xmlns:dc' => 'http://purl.org/dc/elements/1.1/',
        'xmlns:content' => 'http://purl.org/rss/1.0/modules/content/',
        'xmlns:georss' => 'http://www.georss.org/georss' do
  xml.channel 'rdf:about' => root_url do
    title = t 'actus.index.title'
    region = Region.find_by id: params[:region]
    title += " - #{t region.code.presence, scope: :countries, default: region}" if region.present?
    xml.title title
    xml.description t 'layouts.application.subtitle'
    xml.link root_url
    xml.dc :language, 'fr'
    xml.dc :creator, root_url

    xml.items do
      xml.rdf :Seq do
        @actus.each do |actu|
          xml.rdf :li, 'rdf:resource' => actu.link
        end
      end
    end
  end

  @actus.each do |actu|
    xml.item 'rdf:about' => actu.link do
      xml.guid "actu_#{actu.id}@#{request.domain}"
      actu_to_rss xml, actu
    end
  end
end
