json.array!(@events) do |event|
  json.extract! event, :id, :title, :description, :start_time, :end_time,
                :submission_time, :decision_time,
                :place_name, :address, :city, :locality, :url, :contact,
                :region_id
  json.region event.region.try(:name)
  json.url event_url(event, format: :json)
  json.tags event.tags.join ' '
  json.meta event_meta event
end
