def meta(xml, event)
  xml.dc :identifier, "#{event.id}@#{request.domain}"
  xml.dc :date, event.decision_time.iso8601

  return unless event.latitude || event.longitude

  xml.georss :point, event.latitude, ' ', event.longitude
end

def xml_title(event)
  "#{[event.city, event.title].compact.join(': ')}, #{display_date event}"
end

def event_to_rss(xml, event)
  xml.title xml_title event
  xml.pubDate event.decision_time.iso8601
  xml.link event_url event
  xml.description event.description
  xml.content(:encoded) { xml.cdata! event.description }
  xml.hashtags event.hashtags
end

xml.instruct!

xml.rdf :RDF,
        'xmlns' => 'http://purl.org/rss/1.0/',
        'xmlns:rdf' => 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
        'xmlns:dc' => 'http://purl.org/dc/elements/1.1/',
        'xmlns:content' => 'http://purl.org/rss/1.0/modules/content/',
        'xmlns:georss' => 'http://www.georss.org/georss' do
  xml.channel 'rdf:about' => root_url do
    title = t 'layouts.application.title'
    region = Region.find_by id: params[:region]
    title += " - #{t region&.code.presence, scope: :countries, default: region}" if region.present?
    xml.title title
    xml.description t 'layouts.application.subtitle'
    xml.link root_url
    xml.dc :language, 'fr'
    xml.dc :creator, root_url

    xml.items do
      xml.rdf :Seq do
        @events.each do |event|
          xml.rdf :li, 'rdf:resource' => event_url(event)
        end
      end
    end
  end

  @events.each do |event|
    xml.item 'rdf:about' => event_url(event) do
      xml.guid "#{event.id}@#{request.domain}"
      meta xml, event
      event_to_rss xml, event
    end
  end
end
