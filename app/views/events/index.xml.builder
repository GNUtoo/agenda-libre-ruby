xml.instruct!

xml.events do
  @events.find_each do |event|
    xml.event do
      xml.title event.title
      xml.tag! 'start-time', l(event.start_time, format: :xml)
      xml.tag! 'end-time', l(event.end_time, format: :xml)
      xml.place_name event.place_name
      xml.address event.address
      xml.city event.city
      xml.region event.region
      xml.locality event.locality
      xml.tags event.tags.join(' ')
      xml.contact event.contact
      xml.adlurl event_url event
      xml.description { xml.cdata! event.description }
    end
  end
end
