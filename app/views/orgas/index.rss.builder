def meta(xml, orga)
  xml.dc :identifier, "orga_#{orga.id}@#{request.domain}"

  return unless orga.latitude || orga.longitude

  xml.georss :point, orga.latitude, ' ', orga.longitude
end

def orga_to_rss(xml, orga)
  meta xml, orga

  xml.title [orga.city, orga.name].compact.join(': ')
  xml.pubDate orga.created_at.iso8601
  xml.link orga_url orga
  xml.description orga.description
  xml.content(:encoded) { xml.cdata! orga.description || '' }
end

xml.instruct!

xml.rdf :RDF,
        'xmlns' => 'http://purl.org/rss/1.0/',
        'xmlns:rdf' => 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
        'xmlns:dc' => 'http://purl.org/dc/elements/1.1/',
        'xmlns:content' => 'http://purl.org/rss/1.0/modules/content/',
        'xmlns:georss' => 'http://www.georss.org/georss' do
  xml.channel 'rdf:about' => root_url do
    title = t 'orgas.index.title'
    region = Region.find_by id: params[:region]
    title += " - #{t region&.code.presence, scope: :countries, default: region}" if region.present?
    xml.title title
    xml.description t 'layouts.application.subtitle'
    xml.link root_url
    xml.dc :language, 'fr'
    xml.dc :creator, root_url

    xml.items do
      xml.rdf :Seq do
        @orgas.each do |orga|
          xml.rdf :li, 'rdf:resource' => orga_url(orga)
        end
      end
    end
  end

  @orgas.each do |orga|
    xml.item 'rdf:about' => orga_url(orga) do
      xml.guid "orga_#{orga.id}@#{request.domain}"
      orga_to_rss xml, orga
    end
  end
end
