xml.opml version: '2.0' do
  xml.head do
    xml.title t 'layouts.application.title'
    xml.description t 'layouts.application.subtitle'
  end

  xml.body do
    @orgas.select(&:feed).each do |orga|
      xml.outline description: strip_tags(orga.description),
                  htmlUrl: orga.url,
                  text: orga.name,
                  title: orga.name,
                  type: :rss,
                  version: 2.0,
                  xmlUrl: orga.feed
    end
  end
end
