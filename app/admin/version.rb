ActiveAdmin.register PaperTrail::Version, as: 'Version' do
  permit_params :itep_type, :item_id, :whodunnit, :event

  config.filters = false

  actions :index, :show

  index do
    column('When') { |v| l(v.created_at, format: :at) }
    column('Who') do |v|
      link_to(User.find(v.whodunnit).email, [:admin, User.find(v.whodunnit)]) if v.whodunnit.present?
    end
    column('How') { |v| t("#{v.event}_html", scope: 'versions.index') }
    column('Kind') { |v| t(v.item_type.downcase, scope: 'activerecord.models') }
    column('What') do |v|
      link_to v.item, [:admin, v.item] if v.item.present?
    end
    actions
  end
end
