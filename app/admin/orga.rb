ActiveAdmin.register Orga do
  permit_params :active, :kind_id, :name, :address, :city, :region_id,
                :latitude, :longitude, :description, :url, :feed, :contact, :submitter
  sidebar :paper_trail, partial: 'layouts/version', only: :show

  scope :all
  scope :active, default: true
  scope :inactive
  scope :unmoderated

  config.sort_order = 'updated_at_desc'

  filter :kind, as: :check_boxes
  filter :name
  filter :region

  index do
    column :kind
    column :name
    column :city
    column :region
    column :url
    column :feed
    column :contact
    column :submitter
    column :updated_at
    column :active
    actions
  end

  csv do
    column(:kind) { |o| o.kind.name }
    column :name
    column :city
    column :region
    column :url
    column :feed
    column :contact
    column :submitter
    column :updated_at
    column :active
  end

  show do
    attributes_table do
      row :kind
      row :name
      row :address
      row :city
      row :region
      row :description do
        sanitize orga.description, attributes: %w[href src width height style title name id]
      end
      row :url do
        link_to orga.url, orga.url
      end
      row :feed do
        link_to orga.feed, orga.feed
      end
      row :contact do
        mail_to orga.contact, orga.contact
      end
      row :active
    end
    active_admin_comments
  end

  form do |f|
    f.semantic_errors
    f.inputs do
      f.input :kind
      f.input :name
      f.input :address, as: :string
      f.input :city
      f.input :region
      f.input :latitude
      f.input :longitude
      f.input :description
      f.input :url
      f.input :feed
      f.input :contact
      f.input :submitter
      f.input :active
    end
    f.actions
  end
end
