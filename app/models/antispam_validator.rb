# Try to fight spam
class AntispamValidator < ActiveModel::Validator
  # Lazy load and transform directly words to patterns instead of creating regexps at each matching tests
  def blacklist
    @blacklist ||= YAML.load_file(blacklist_file).map(&:downcase)
  end

  def validate(record)
    options[:fields].filter { |field| blacklisted? record.send(field) }
                    .filter { |field| new_record? record, field }
                    .each { |field| record.errors.add field, 'blacklisted (currently lot of spam)' }
  end

  private

  def blacklisted?(field)
    blacklist.any? { |word| field&.downcase&.include? word }
  end

  def blacklist_file
    if defined?(Rails.root) && (blacklist_file_path = Rails.root.join('config/blacklist.yml')).exist?
      return blacklist_file_path
    end

    File.join(File.dirname(__FILE__), '../config/blacklist.yml')
  end

  # Check this blacklisted field is not already in database
  def new_record?(record, field)
    !record.class.exists? field.to_sym => record.send(field)
  end
end
