# Store feed items
class Actu < ApplicationRecord
  belongs_to :orga

  def self.ransackable_associations(_auth_object = nil)
    %w[orga]
  end

  def self.ransackable_attributes(_auth_object = nil)
    %w[orga_id guid title link description created_at published_at]
  end

  validates :title, presence: true
  validates :description, presence: true
  validates :published_at, presence: true

  default_scope { includes(orga: %i[kind region]) }
  scope :active, -> { where('orga.active': true) }
  scope :city, ->(city) { where(orga: Orga.city(city)) }
  scope :region, ->(region) { where(orga: Orga.region(region)) }
  scope :tag, ->(tag) { where(orga: Orga.tag(tag)) }
  scope :near, ->(loc, dist) { where(orga: Orga.near(loc, dist).map(&:id)) }
  scope :orga, ->(orga) { where(orga: orga) }

  before_create do
    self.published_at = created_at if published_at.future?
  end

  def to_s
    title
  end
end
