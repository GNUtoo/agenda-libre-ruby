require 'digest/md5'

# Moderators, but using a failed pwd mechanism
# TODO, migrate to full active_admin
class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable,
         authentication_keys: [:login]

  def self.ransackable_attributes(_auth_object = nil)
    %w[login email firstname lastname]
  end

  validates :login, presence: true

  def encrypted_password=(pass)
    self[:password] = pass
  end

  def encrypted_password
    self[:password]
  end

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    login = conditions.delete(:login)
    if login.present?
      where(conditions).find_by login: login
    else
      where(conditions).limit(1).to_a[0]
    end
  end

  def valid_password?(password)
    encrypted_password == password_digest(password)
  end

  def to_s
    if firstname.present? || lastname.present?
      [firstname, lastname].join ' '
    else
      login
    end
  end

  protected

  def password_digest(password)
    Digest::MD5.hexdigest password
  end
end
