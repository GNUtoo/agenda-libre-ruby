# Gives the possibility to organise organisations! :)
class Kind < ApplicationRecord
  has_many :orgas, dependent: :destroy

  def self.ransackable_attributes(_auth_object = nil)
    %w[orgas_id name icon]
  end
end
