# This is mostly to group events around a region
class Region < ApplicationRecord
  belongs_to :region, optional: true

  has_many :regions, dependent: :nullify

  has_many :orgas, dependent: :destroy

  def self.ransackable_associations(_auth_object = nil)
    %w[region]
  end

  def self.ransackable_attributes(_auth_object = nil)
    %w[region_id regions_id orgas_id code name url]
  end

  default_scope { order :name }
  scope :top, -> { where(region: nil).includes(:regions) }
  scope :local, -> { where(url: [nil, '']) }
  scope :region, (lambda do |region|
    return if region.nil? || region == 'all' || region.to_i.zero?

    temp = Region.find region
    where region: [temp, temp.regions].flatten
  end)

  def to_s
    name
  end

  def flag
    region&.flag || code&.downcase
  end

  # Attempt to get a corresponding timezone, used for ical
  def tzid
    country = TZInfo::Country.get region.try(:code) || code
    if country.present? && country.zone_identifiers.length.positive?
      # Get arbitrarily the first zone for this country
      country.zone_identifiers[0]
    else
      Time.now.zone
    end
  rescue TZInfo::InvalidCountryCode
    # Mostly useful for non country calendars
    Time.now.zone
  end
end
