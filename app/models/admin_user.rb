# The new agenda moderators, from active_admin
class AdminUser < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable

  def self.ransackable_attributes(_auth_object = nil)
    %w[email]
  end
end
