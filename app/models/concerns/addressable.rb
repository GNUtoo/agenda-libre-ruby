# Factorise some common address behaviour
module Addressable
  extend ActiveSupport::Concern

  included do
    belongs_to :region

    scope :city, ->(city) { where city: city }
    scope :region, (lambda do |region|
      # Find all from a region or a "sub-region"
      where region: Region.where(id: region).or(Region.where(region: region))
    end)
    scope :geo, -> { where 'latitude is not null and longitude is not null' }

    before_validation on: :update do
      self.latitude = nil if address_changed?
      self.longitude = nil if address_changed?
    end

    geocoded_by :full_address

    after_validation :geocode, if: (lambda do |obj|
      obj.address_changed? || obj.city_changed? || latitude.nil?
    end)
  end

  def full_address
    # Only uses the region if it is a sub-region and not a country
    [address, city, region.try(:region).present? ? region : nil,
     region.try(:region)].compact.join ', '
  end
end
