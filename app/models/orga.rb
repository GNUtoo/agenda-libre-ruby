# Organisations related to this agenda
class Orga < ApplicationRecord
  include Addressable

  acts_as_taggable
  strip_attributes
  has_paper_trail ignore: %i[last_updated secret submitter decision_time
                             lock_version latitude longitude]

  belongs_to :kind
  has_many :actus, dependent: :destroy

  def self.ransackable_associations(_auth_object = nil)
    %w[kind region actus]
  end

  def self.ransackable_attributes(_auth_object = nil)
    %w[name kind_id region_id active city description updated_at]
  end

  validates_with AntispamValidator, fields: %i[name submitter], on: :create

  validates :name, presence: true
  validates :url, format: %r{\Ahttps?://.*\..*\z}
  validates :diaspora, allow_blank: true, format: %r{\Ahttps?://.*\..*\z}
  validates :feed, allow_blank: true, format: %r{\Ahttps?://.*\..*\z}
  validates :contact, allow_blank: true, email: true
  validates :submitter, allow_blank: true, email: true

  default_scope { includes :region, :kind, :tags }
  scope :active, ->(value = true) { where active: value }
  scope :inactive, ->(value = false) { where active: value }
  scope :moderated, -> { where moderated: true }
  scope :unmoderated, -> { where moderated: false }
  scope :with_feed, -> { where.not(feed: '') }

  # Only present to simplify maps_controller, to have the same scopes as events
  scope :locality, ->(_locality) {}
  scope :daylimit, ->(_nb) {}
  scope :future, -> {}
  scope :period, ->(_year, _week) {}
  scope :kind, ->(kind) { where kind: kind }
  scope :tag, ->(tag) { tagged_with tag }

  before_validation do
    self.secret ||= SecureRandom.urlsafe_base64(32)[0...32]
    self.submission_time ||= Time.zone.now
  end

  after_create do
    send_secret
    # Send email to moderators when an new orga is received
    ModerationorgaMailer.create(self).deliver_now!
  end

  after_update do
    send_secret if saved_change_to_secret? || saved_change_to_submitter?

    if saved_change_to_moderated?
      # Send an acceptation mail to its author
      OrgaMailer.accept(self).deliver_now! if submitter.present?

      # Send an acceptation mail to moderators
      ModerationorgaMailer.accept(self).deliver_now

    else
      # Send an update mail to its author
      OrgaMailer.update(self).deliver_now! if submitter.present?

      # Send an update mail to moderators
      ModerationorgaMailer.update(self).deliver_now
    end
  end

  before_destroy do
    OrgaMailer.destroy(self).deliver_now! if submitter.present?
    # Send email to moderators when deleted
    ModerationorgaMailer.destroy(self).deliver_now!
  end

  def send_secret
    OrgaMailer.create(self).deliver_now! if submitter.present? && secret.present?
  end

  def name_as_tag
    name.delete_prefix('L\'').gsub(/[\s*'.]/, '-').delete ':'
  end

  def to_s
    name
  end
end
