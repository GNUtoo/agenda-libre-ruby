require 'schedule'

# This is the central ADL class, where are managed all events
class Event < ApplicationRecord
  extend SimpleCalendar
  include Addressable
  include Schedule

  acts_as_taggable
  strip_attributes
  has_paper_trail ignore: %i[last_updated lock_version secret submitter
                             decision_time latitude longitude]

  # This is the eventual scheduled first event
  belongs_to :event, optional: true
  has_many :notes, dependent: :destroy
  has_many :events, dependent: :destroy

  def self.ransackable_associations(_auth_object = nil)
    %w[region]
  end

  def self.ransackable_attributes(_auth_object = nil)
    %w[region_id title city description]
  end

  validates_with AntispamValidator, fields: %i[title submitter], on: :create

  validates :title, presence: true
  validate :end_after_start
  RULES = %w[daily weekly monthly monthly_day].freeze
  validates :rule, inclusion: RULES, allow_nil: true
  validates :description, presence: true
  validates :url, allow_nil: true, format: %r{\Ahttps?://.*\..*\z}
  validates :contact, email: true, allow_nil: true
  validates :submitter, email: true

  # Mechanism to store some reason which can be used when sending notifications
  attr_accessor :reason

  before_create EventCallbacks
  after_create EventCallbacks

  before_update EventCallbacks
  after_update EventCallbacks

  after_destroy EventCallbacks

  scope :moderated, ->(*) { where moderated: true }
  scope :unmoderated, ->(*) { where moderated: false }
  scope :past, -> { where 'start_time <= ?', Time.zone.now }
  scope :future, -> { where '? <= end_time', Time.zone.now }
  scope :daylimit, ->(nb) { where 'end_time <= ?', nb.to_i.days.from_now }
  scope :year, (lambda do |year|
    where '? <= end_time and start_time <= ?',
          Date.new(year.to_i, 1, 1).beginning_of_week,
          Date.new(year.to_i, 12, 31).end_of_week.end_of_day
  end)
  scope :start_date, (lambda do |start_date|
    where '? <= end_time and start_time <= ?',
          (start_date || Time.zone.today).to_date.beginning_of_month.beginning_of_week,
          (start_date || Time.zone.today).to_date.end_of_month.end_of_week.end_of_day
  end)
  scope :period, (lambda do |year, week|
    start_date = Date.commercial(
      year.to_i, (week || (Time.zone.today + 7.days).cweek).to_i
    )
    where '? <= end_time and start_time <= ?',
          start_date, start_date.end_of_week.end_of_day
  end)
  scope :locality, ->(locality) { where locality: locality }
  scope :tag, ->(tag) { tagged_with tag }

  before_validation on: :create do
    self.submission_time = Time.zone.now
    self.decision_time = Time.zone.now
  end

  def hashtags
    tags.map { |tag| "##{tag.name.tr('-', '_').camelize :lower}" }.join ' '
  end

  def to_s
    [start_time.to_date, city ? "#{city}:" : nil, title, hashtags].compact.join(' ')
  end

  private

  def end_after_start
    errors.add :end_time, :before_start if end_time && start_time && end_time < start_time
  end
end
