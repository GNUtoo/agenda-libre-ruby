# Groups life cycle
class OrgasController < ApplicationController
  has_scope :city, :region, :tag
  has_scope :near, type: :hash, using: %i[location distance]
  has_scope :active, type: :boolean, default: true, allow_blank: true

  before_action :set_orga, if: -> { params[:id].present? }
  before_action :authenticate_user!, if: -> { params[:id].present? },
                                     unless: :check_secret,
                                     except: %i[show]

  def index
    @unfiltered_orgas = apply_scopes Orga.moderated
    @search = @unfiltered_orgas.ransack params[:q]
    @search.sorts = 'updated_at desc' if @search.sorts.empty?
    @orgas = @search.result
    @orgas = @orgas.page(params[:page]).without_count unless params[:format] == 'opml'
  end

  def show
    @events_future = Event.moderated.future.tag @orga.name_as_tag
    @events_past = Event.moderated.past.tag @orga.name_as_tag
  end

  # GET /orgas/new
  def new
    @orga = Orga.new region_id: params[:region]
  end

  # POST /orgas/preview
  def preview_create
    @orga = Orga.new orga_params
    @orga.valid?
    render action: :new
  end

  # POST /orgas
  # POST /orgas.json
  def create
    @orga = Orga.new orga_params
    respond_to do |format|
      if @orga.save
        format.html { redirect_to orga_path(@orga, secret: @orga.secret), notice: t('.ok') }
        format.json { render action: :show, status: :created, location: @orga }
      else
        format.html { render action: :new }
        format.json { render json: @orga.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orgas/1/preview
  def preview
    @orga.attributes = orga_params
    @orga.valid?
    render action: :edit
  end

  # PATCH/PUT /orgas/1
  # PATCH/PUT /orgas/1.json
  def update
    respond_to do |format|
      if @orga.update orga_params
        format.html { redirect_to @orga, notice: t('.ok') }
        format.json { head :no_content }
      else
        format.html { render action: :edit }
        format.json { render json: @orga.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /accept/1
  # PATCH/PUT /accept/1.json
  def accept
    @orga.update moderated: true
    respond_to do |format|
      format.html { redirect_to :moderations, notice: t('.ok') }
      format.json { head :no_content }
    end
  end

  # DELETE /orgas/1
  # DELETE /orgas/1.json
  def destroy
    @orga.destroy
    respond_to do |format|
      format.html { redirect_to :moderations, notice: t('.ok') }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_orga
    @orga = Orga.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through
  def orga_params
    params.require(:orga)
          .permit :lock_version, :kind_id, :name, :description, :place_name,
                  :address, :city, :department, :region_id, :url, :diaspora,
                  :feed, :contact, :submitter, :tag_list, :active
  end

  # Check that you can only edit an existing event if you know its secret
  def check_secret
    %w[validate refuse].exclude?(action_name) &&
      (!@orga.secret || @orga.secret == params[:secret])
  end
end
