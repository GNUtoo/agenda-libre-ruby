# Event life cycle
# This is a central part to this project
class EventsController < ApplicationController
  has_scope :start_date, default: nil, allow_blank: true, if: :monthly?
  has_scope :city, :region, :locality, :tag, :daylimit, :year
  has_scope :near, type: :hash, using: %i[location distance]
  has_scope :future, type: :boolean, default: true, only: [:index], if: :future?

  before_action :set_event, if: -> { params[:id].present? }
  before_action :check_secret, only: %i[edit preview update destroy]
  skip_before_action :verify_authenticity_token, only: [:preview_create]
  rescue_from ActiveRecord::StaleObjectError, with: :locked

  def index
    # The 3000 limit is purely arbitrary...
    @events = apply_scopes(Event).moderated.includes(:tags).limit(3000).order(start_time: :asc)
    @events = @events.limit(20).order(id: :desc) if params[:format] == 'rss'
  end

  # GET /events/new
  def new
    @event = Event.new region_id: params[:region]
  end

  def edit; end

  # GET/POST /events/preview
  def preview_create
    @event = Event.new event_params
    @event.valid?
    render action: :new
  end

  # POST /events
  # POST /events.json
  def create
    @event = Event.new event_params
    respond_to do |format|
      if @event.save
        format.html { redirect_to event_path(@event, secret: @event.secret), notice: t('.ok') }
        format.json { render action: :show, status: :created, location: @event }
      else
        format.html { render action: :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1/preview
  def preview
    @event.attributes = event_params
    @event.valid?
    render action: :edit
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    respond_to do |format|
      if @event.update event_params
        format.html { redirect_to @event, notice: t('.ok') }
        format.json { head :no_content }
      else
        format.html { render action: :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.reason = params[:event][:reason]
    @event.destroy
    respond_to do |format|
      format.html { redirect_to :root, notice: t('.ok') }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions
  def set_event
    @event = Event.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through
  def event_params
    params.require(:event)
          .permit :lock_version, :title, :start_time, :end_time, :repeat, :rule,
                  :description, :place_name, :address, :city, :region_id,
                  :locality, :url, :contact, :submitter, :tag_list
  end

  def locked
    redirect_to edit_event_url(@event, secret: @event.secret),
                alert: t('staleObjectError')
  end

  # Check that you can only edit an existing event if you know its secret
  def check_secret
    redirect_to :root, alert: t('events.edit.forbidden') \
      unless params[:secret] == @event.secret
  end

  # Should the future scope be applied?
  # Only on non html pages...
  def future?
    params[:format].present? && params[:format] != 'html'
  end

  # To limit the results to a month view
  def monthly?
    (params[:format].blank? || params[:format] == 'html') && params[:year].blank?
  end
end
