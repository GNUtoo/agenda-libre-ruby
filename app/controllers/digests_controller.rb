# A digest of all events over a period of time
class DigestsController < ApplicationController
  has_scope :region, :tag
  has_scope :near, type: :hash, using: %i[location distance]
  has_scope :period, allow_blank: true, type: :hash, using: %i[year week],
                     default: (
                       lambda do
                         { year: (Time.zone.today + 7.days).year,
                           week: (Time.zone.today + 7.days).cweek }
                       end)

  before_action :set_week, if: -> { params[:period].present? && params[:period][:week].present? }

  def show
    @week ||= Time.zone.today + 7.days
    @events = apply_scopes Event.moderated
    render :markdown
  end

  private

  def set_week
    @week = Date.commercial params[:period][:year].to_i,
                            params[:period][:week].to_i
  end
end
