# Generate statistics, around events, by date or place
class StatsController < ApplicationController
  has_scope :city, :region, :tag
  has_scope :near, type: :hash, using: %i[location distance]

  before_action :set_events, :set_cities, :set_tags, :counts, :temporal,
                :set_regions, only: [:index]

  private

  def counts
    @events_count = @events.count :all
    @events_um_count = apply_scopes(Event.unmoderated).count :all
    @orgas_count = apply_scopes(Orga).moderated.count :all
    @orgas_um_count = apply_scopes(Orga.unmoderated).count :all
    @actus_count = apply_scopes(Actu).count :all
  end

  def temporal
    @years = @events.group(year_grouping).count :all
    @months = @events.group(year_grouping, month_grouping).count :all
  end

  def set_regions
    @region_events = @events.group(:region_id, year_grouping).count :all
    @regions = Region.all.find_all do |region|
      @years.sum { |year| @region_events[[region.id, year[0]]] || 0 }.positive?
    end
  end

  def set_events
    # Remove the ordering which can occur after geocoding
    @events = apply_scopes(Event.unscoped.moderated).reorder nil
  end

  def set_cities
    @cities = @events.where.not(city: nil)
                     .group(:city)
                     .having('count(city) > ?', Rails.configuration.cloud_threshold)
                     .order('count(city) desc')
                     .count(:all)
                     .then { |h| h.keys.group_by { |k| h[k] } }
                     .sort
                     .reverse
  end

  def set_tags
    @tags = @events.tag_counts_on(:tags)
                   .where('taggings_count > ?', Rails.configuration.cloud_threshold)
  end

  def year_grouping
    if %w[Mysql2 MySQL PostgreSQL].include? Event.connection.adapter_name
      'extract(year from start_time)'
    elsif Event.connection.adapter_name == 'SQLite'
      "strftime('%Y', start_time)"
    end
  end

  def month_grouping
    if %w[Mysql2 MySQL PostgreSQL].include? Event.connection.adapter_name
      'extract(month from start_time)'
    elsif Event.connection.adapter_name == 'SQLite'
      "strftime('%m', start_time)"
    end
  end
end
