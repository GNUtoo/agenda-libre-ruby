# The top level controller, where can be centralised almost everything
class ApplicationController < ActionController::Base
  include HttpAcceptLanguage::AutoLocale

  # Important to be present before the following "before_action"
  preserve :near, :region, :tag, allow_blank: true

  before_action :set_paper_trail_whodunnit,
                :discard,
                :set_mailer_host,
                :calculate_digest_counts
  before_action :configure_permitted_parameters, if: :devise_controller?

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery prepend: true, with: :exception

  layout :handle_xhr_layout

  private

  # Mechanism to manage filter resets
  def discard
    params.compact_blank!
    session.keys
           .keep_if { |key| session[key].blank? }
           .each { |key| session.delete key }
  end

  def calculate_digest_counts
    @events_digest_count = Event.moderated.future.count
    @orgas_digest_count = Orga.moderated.active.count
    @actus_digest_count = Actu.active.count
  end

  protected

  # Useful to manage absolute url in mails
  def set_mailer_host
    ActionMailer::Base.default_url_options[:host] = request.host_with_port
  end

  # If the request is an xhr, we don't render any layout
  def handle_xhr_layout
    request.xhr? ? false : 'application'
  end

  # Add some parameters to the list of default devise ones
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit :account_update,
                                      keys: %i[login email firstname lastname]
  end
end
