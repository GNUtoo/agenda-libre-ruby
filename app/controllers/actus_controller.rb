# Display organisations' actualities
class ActusController < ApplicationController
  has_scope :city, :region, :tag, :orga
  has_scope :near, type: :hash, using: %i[location distance]

  def index
    @unfiltered_actus = apply_scopes(Actu.active)
    @search = @unfiltered_actus.ransack params[:q]
    @search.sorts = 'published_at desc' if @search.sorts.empty?
    @actus = @search.result
    @actus = @actus.page(params[:page]).without_count
  end
end
