# Manage event and organisation tags
class TagsController < InheritedResources::Base
  has_scope :region, :locality, :daylimit
  has_scope :tag, as: :id
  before_action :set_orgas, if: -> { params[:id].present? }

  def index
    @tags = ActsAsTaggableOn::Tag.where('taggings_count > ?',
                                        Rails.configuration.cloud_threshold)
    if params[:term]
      # Used to autocomplete tags
      @tags = @tags.select(:id, :name, 'name AS label')
                   .where('name LIKE ?', "#{params[:term]}%")
    end

    respond_to do |format|
      format.html
      format.json { render json: @tags }
    end
  end

  def show
    @futures = apply_scopes Event.reorder('events.start_time').moderated.future.includes(:tags)
    @pasts = apply_scopes Event.reorder('events.start_time DESC').moderated.past.includes(:tags)

    respond_to do |format|
      format.html
      format.json { render json: @futures + @pasts }
    end
  end

  private

  # Splits, groups, rejects the less used
  def set_orgas
    @orgas = (apply_scopes(Orga.moderated.active) +
              Orga.moderated.active
                 .where(
                   'lower(orgas.name) like lower(?)',
                   "%#{params[:id].tr '-', '%'}%"
                 )
             ).uniq
  end
end
