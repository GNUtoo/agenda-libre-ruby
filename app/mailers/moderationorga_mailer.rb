# Send mails to check on organisations life cycle
class ModerationorgaMailer < ApplicationMailer
  helper :events

  def create(orga)
    @orga = orga

    mail 'Message-ID' =>
      "<orga-#{orga.id}@#{ActionMailer::Base.default_url_options[:host]}>",
         subject: subject(orga),
         'Content-Language': locale
  end

  def update(orga)
    @orga = orga
    @current_user = User.find_by id: orga.paper_trail.originator

    mail 'In-Reply-To' =>
      "<orga-#{orga.id}@#{ActionMailer::Base.default_url_options[:host]}>",
         subject: subject(orga),
         'Content-Language': locale
  end

  def accept(orga)
    @orga = orga
    @current_user = User.find_by id: orga.paper_trail.originator

    mail 'In-Reply-To' =>
      "<orga-#{orga.id}@#{ActionMailer::Base.default_url_options[:host]}>",
         subject: subject(orga),
         'Content-Language': locale
  end

  def destroy(orga, reason = '')
    @orga = orga
    @current_user = User.find_by id: orga.paper_trail.originator
    @reason = reason

    mail 'In-Reply-To' =>
      "<orga-#{orga.id}@#{ActionMailer::Base.default_url_options[:host]}>",
         subject: subject(orga),
         'Content-Language': locale
  end

  private

  def subject(orga)
    t('mail_prefix') +
      t("#{mailer_name}.#{action_name}.subject", subject: orga.name)
  end
end
