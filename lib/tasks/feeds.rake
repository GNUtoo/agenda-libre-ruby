require 'rss'

module RSS
  class Rss
    class Channel
      # Override to have some specific transformative methods
      class Item
        def guid
          link
        end

        def to_actu(orga)
          Actu.new orga: orga,
                   guid: guid,
                   title: title,
                   link: link,
                   description: description,
                   published_at: pubDate || dc_date
        end
      end
    end
  end

  class RDF
    # Specific override for some rdf feeds
    class Item
      def guid
        link
      end

      def to_actu(orga)
        Actu.new orga: orga,
                 guid: guid,
                 title: title,
                 link: link,
                 description: description,
                 published_at: date
      end
    end
  end

  module Atom
    class Feed
      # Override to have some specific transformative methods
      class Entry
        def guid
          id.content
        end

        def description
          content.try(:content) || content || summary.try(:content) || summary
        end

        def published_at
          published.try(:content) || published || updated.try(:content) || updated
        end

        def to_actu(orga)
          Actu.new orga: orga,
                   guid: guid,
                   title: title.content,
                   link: link.href,
                   description: description,
                   published_at: published_at
        end
      end
    end
  end
end

def parse(orga, domain)
  RSS::Parser.parse(orga.feed, false).items
             .map { |item| item.to_actu orga }
             .reject { |actu| Actu.exists?(guid: actu.guid) || actu.invalid? }
             .reverse_each(&:save)
rescue StandardError => e
  puts "#{domain}/orgas/#{orga.id} #{orga.name} => #{e.message}"
end

namespace :feeds do
  desc 'Fetch organisations feeds'
  task :fetch, [:domain] => :environment do |_tags, args|
    # So that the recorded time zone correspond to the entities'
    ActiveRecord.default_timezone = :local
    Orga.moderated.active.with_feed.each { |orga| parse orga, args.domain || 'http://localhost:3000' }
  end

  desc 'Test a specific URL'
  task :test, [:url] => :environment do |_tags, args|
    RSS::Parser.parse(args.url, false).items
               .map { |item| item.to_actu Orga.first }
               .reject(&:validate)
               .each { |actu| puts actu.errors.full_messages }
  rescue StandardError => e
    puts e.message
  end
end
