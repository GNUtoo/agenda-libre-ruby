(function() {
  $(document).on('turbolinks:load', function() {
    $('table.list.dates tbody .sparkline').each(function() {
      var vals;
      vals = $(this).closest('tr').find('.quantity').map(function() {
        var ref;
        return ((ref = $(this).text()) != null ? ref.replace(' ', '') : void 0) || 0;
      });
      return $(this).sparkline(vals);
    });
    return $('table.list.dates tfoot .sparkline').each(function() {
      var vals;
      vals = $(this).closest('tfoot').find('.quantity').map(function() {
        return $(this).text().replace(' ', '') || 0;
      });
      return $(this).sparkline(vals, {
        type: 'bar'
      });
    });
  });

}).call(this);
