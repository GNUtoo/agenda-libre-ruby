(function() {
  $(document).on('turbolinks:load', function() {
    return tinyMCE.init({
      width: '100%',
      height: '40em',
      menubar: false,
      branding: false,
      language: 'fr_FR',
      selector: 'textarea.description',
      content_css: '/assets/application-2c8a4bb0d97841e3a1a73ef5a6273fa7e227843567b1113dfec7a79d776388ba.css',
      relative_urls: false,
      entity_encoding: 'raw',
      document_base_url: '/',
      paste_data_images: false,
      add_unload_trigger: true,
      browser_spellcheck: true,
      style_formats_autohide: true,
      toolbar: [' cut copy paste | undo redo | link image media charmap table | code visualblocks searchreplace', ' removeformat bold italic strikethrough superscript subscript blockquote forecolor backcolor | bullist numlist outdent indent | alignleft aligncenter alignright alignjustify alignnone'],
      toolbar_sticky: true,
      plugins: 'lists advlist autolink link image charmap preview table fullscreen searchreplace media insertdatetime visualblocks wordcount code'
    });
  });

  $(document).on('turbolinks:before-cache', function() {
    return tinymce.remove();
  });

}).call(this);
