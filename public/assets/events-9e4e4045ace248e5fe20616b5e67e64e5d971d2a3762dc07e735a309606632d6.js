(function() {
  $(document).on('turbolinks:load', function() {
    $('#event_start_time').change(function() {
      if ($('#event_start_time').val() >= $('#event_end_time').val()) {
        return $('#event_end_time').val($('#event_start_time').val());
      }
    });
    $('#event_end_time').change(function() {
      if (!$('#event_start_time').val()) {
        return $('#event_start_time').val($('#event_end_time').val());
      }
    });
    if (!Modernizr.inputtypes['datetime-local'] && Modernizr.inputtypes.date && Modernizr.inputtypes.time) {
      $('[type=datetime-local]').hide().after(function() {
        return "<input type='time' required value='" + (this.value && new Date(this.value).toTimeString().split(' ')[0]) + "'></input>";
      }).after(function() {
        return "<input type='date' required value='" + (this.value && new Date(this.value).toISOString().split('T')[0]) + "'></input>";
      });
      $('[type=date], [type=time]').css('flex-grow', 0).css('margin', 0).change(function() {
        var date, target, time;
        date = $(this).parent().find('[type=date]');
        time = $(this).parent().find('[type=time]');
        if (!date.val() || !time.val()) {
          return;
        }
        target = $(this).parent().find('[type=datetime-local]');
        return target.val(date.val() + 'T' + time.val());
      });
    }
    $('#event_repeat').each(function() {
      if ($(this).val() === '0') {
        $('.field.rule').hide();
      }
      return $(this).change(function() {
        if ($(this).val() > 0) {
          $('.field.rule').show();
          return $('.field.rule input').attr('required', 'required');
        } else {
          $('.field.rule').hide();
          return $('.field.rule input').removeAttr('required');
        }
      });
    });
    return $('#event_tags').each(function() {
      var elt;
      elt = $(this);
      return $.ajax({
        url: '/tags.json'
      }).done(function(data) {
        var tags;
        return tags = jQuery.map(data, function(n) {
          return n[0];
        });
      });
    });
  });

}).call(this);
