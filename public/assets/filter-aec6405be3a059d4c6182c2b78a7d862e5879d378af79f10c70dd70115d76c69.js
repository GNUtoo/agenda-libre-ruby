(function() {
  $(document).on('turbolinks:load', function() {
    $('body.pages form :input').prop('disabled', false);
    return $('form').submit(function() {
      $('input[name=utf8]').prop('disabled', true);
      return $('button').prop('disabled', true);
    });
  });

}).call(this);
