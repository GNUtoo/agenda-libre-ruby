(function() {
  $(document).on('turbolinks:load', function() {
    return tinyMCE.init({
      width: '100%',
      height: '40em',
      menubar: false,
      branding: false,
      language: 'fr_FR',
      selector: 'textarea.description',
      content_css: '/assets/application-b40d0aed5f1e24c0ccced44247a8ef3b93d6aa1e960e13be0f332f6f0ec4a623.css',
      relative_urls: false,
      entity_encoding: 'raw',
      document_base_url: '/',
      add_unload_trigger: true,
      browser_spellcheck: true,
      style_formats_autohide: true,
      toolbar: [' cut copy paste | undo redo | link image media charmap table | code visualblocks searchreplace', ' removeformat bold italic strikethrough superscript subscript | bullist numlist outdent indent | alignleft aligncenter alignright alignjustify alignnone'],
      toolbar_sticky: true,
      plugins: 'lists advlist autolink link image charmap preview table fullscreen searchreplace media insertdatetime visualblocks wordcount code'
    });
  });

  $(document).on('turbolinks:before-cache', function() {
    return tinymce.remove();
  });

}).call(this);
